from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from receipts.models import Receipt
from django.contrib.auth.mixins import LoginRequiredMixin
# Create your views here.

class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipt_list/home.html"
    context_object_name = "favoritereceipts"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)

class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt

    def form_valid(self, form):
        form.instance.purchaser = self.request.user
        return super().form_valid(form)

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)

class ExpenseCreateView(LoginRequiredMixin, CreateView):
    model = Receipt

    def form_valid(self, form):
        form.instance.purchaser = self.request.user
        return super().form_valid(form)

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)

class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipt_list/categories.html"
    context_object_name = "expenselist"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)

class AccountListView(LoginRequiredMixin, ListView):
    model = Receipt
    template = "receipt_list/accounts.html"
    context_object_name = "accountlist"


    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)