from django.urls import path
from receipts.views import (
    ReceiptListView,
    ReceiptCreateView,
    ExpenseCreateView,
    ExpenseCategoryListView,
    AccountListView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path("categories/create/", ExpenseCreateView.as_view(), name="create_category"),
    path("categories/", ExpenseCategoryListView.as_view(), name="create_expense"),
    path("accounts/", AccountListView.as_view(), name="create_account"),
]